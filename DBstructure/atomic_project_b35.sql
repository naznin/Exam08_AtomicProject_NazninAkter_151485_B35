-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2016 at 02:30 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b35`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthdate`
--

CREATE TABLE `birthdate` (
  `id` int(11) NOT NULL,
  `person_name` varchar(111) NOT NULL,
  `birthdate` date NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthdate`
--

INSERT INTO `birthdate` (`id`, `person_name`, `birthdate`, `is_deleted`) VALUES
(2, 'Naznin Akter', '1990-11-04', 0),
(4, 'Shehjin', '2002-04-29', 0),
(5, 'Naznin Akter', '0000-00-00', 0),
(6, 'bv', '0000-00-00', 0),
(7, 'bv', '0000-00-00', 0),
(8, 'df', '0000-00-00', 0),
(9, 'fghjj', '2016-11-10', 0),
(10, 'kjjkll', '2016-11-19', 0),
(11, 'jkkj', '2016-11-23', 0),
(12, 'hj', '2016-11-02', 0),
(14, 'ghhj', '2016-11-24', 0),
(15, 'op', '2016-11-30', 0),
(19, 'kkk', '2016-11-02', 0),
(20, 'kl', '2016-11-01', 0),
(21, 'ghs', '2016-11-03', 0),
(22, 'jkklll', '2016-11-09', 0);

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_title` varchar(111) NOT NULL,
  `author_name` varchar(111) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_deleted`) VALUES
(1, 'The Alchemist', 'Paulo coelho', 0),
(2, 'Shesher kobita', 'Rabindranath Tagore', 0),
(3, 'Around the world in eighty days', 'Jule verne', 0),
(5, 'Anil Bagchir Ekdin', 'Humayun Ahmed', 2147483647);

-- --------------------------------------------------------

--
-- Table structure for table `city_name`
--

CREATE TABLE `city_name` (
  `id` int(33) NOT NULL,
  `person_name` varchar(111) NOT NULL,
  `city_name` varchar(111) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city_name`
--

INSERT INTO `city_name` (`id`, `person_name`, `city_name`, `is_deleted`) VALUES
(1, 'Naznin', 'Chittagong', 0),
(2, 'Shehjin', 'Chittagong', 0),
(3, 'Shakil', 'Dhaka', 0),
(4, 'Selim', 'Natore', 0);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(33) NOT NULL,
  `person_name` varchar(111) NOT NULL,
  `email_id` varchar(111) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `person_name`, `email_id`, `is_deleted`) VALUES
(1, 'Naznin', 'akternaznin32@gmail.com', 0),
(2, 'Sajedul Hoque', 'hoque.cse@cu.ac.bd', 0),
(3, 'Rashed Mustafa ', 'rashed_mustafa78@yahoo.com', 0),
(4, 'Sanaullah Chowdhury', 'sana1691@gmail.com', 0),
(5, 'naznin', 'akternaznin60@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(33) NOT NULL,
  `person_name` varchar(111) NOT NULL,
  `gender` varchar(111) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `person_name`, `gender`, `is_deleted`) VALUES
(1, 'Naznin', 'Female', 0),
(2, 'Shehjin', 'Female', 0),
(3, 'shakil', 'Male', 0),
(4, 'Selim', 'Male', 0);

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `person_name` varchar(222) NOT NULL,
  `hobbies` varchar(222) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `person_name`, `hobbies`, `is_deleted`) VALUES
(1, 'er', '', 0),
(2, 'rt', 'Array', 0),
(3, 'ty', '', 0),
(4, 'as', 'Array', 0),
(5, 'hj', 'Cycling', 0),
(6, 'rtt', 'Cycling,reading books ,drawing', 0),
(7, 'Shehjin', 'Cycling,reading books ,drawing,origami', 0);

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(100) NOT NULL,
  `person_name` varchar(111) NOT NULL,
  `profile_picture` varchar(111) NOT NULL,
  `is_deleted` varchar(2) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `person_name`, `profile_picture`, `is_deleted`) VALUES
(14, 'Shila', '1479661373184349_10151510940734603_1988006778_n.jpg', 'No'),
(15, 'Selim', '147966191675baa_flowers_5602939665_c3f1be7cd0.jpg', 'No'),
(16, 'Naznin', '147966193012QBMGM30-92V0.jpg', 'No'),
(17, 'Shehjin', '147969120875baa_flowers_5602939665_c3f1be7cd0.jpg', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE `summary_of_organization` (
  `id` int(22) NOT NULL,
  `organization_name` varchar(3000) NOT NULL,
  `organization_summary` varchar(3000) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`id`, `organization_name`, `organization_summary`, `is_deleted`) VALUES
(1, 'BITM', 'Skill Development', 0),
(2, 'LICT', 'IT training', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthdate`
--
ALTER TABLE `birthdate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city_name`
--
ALTER TABLE `city_name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthdate`
--
ALTER TABLE `birthdate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `city_name`
--
ALTER TABLE `city_name`
  MODIFY `id` int(33) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(33) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(33) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  MODIFY `id` int(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
