<?php
namespace App\Birthdate;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO ;

class Birthdate extends DB{
    public $id="";
    public $person_name="";
    public $birthdate="";

    public function __construct(){
    parent:: __construct();
    if(!isset( $_SESSION)) session_start();
}

    public function setData($postVariableData=NULL){

        if(array_key_exists('id',$postVariableData)){
            $this->id = $postVariableData['id'];
        }

        if(array_key_exists('person_name',$postVariableData)){
            $this->person_name = $postVariableData['person_name'];
        }

        if(array_key_exists('birthdate',$postVariableData)){
            $this->birthdate = $postVariableData['birthdate'];
        }
    }



    public function store(){

    $arrData = array( $this->person_name, $this->birthdate);

    $sql = "Insert INTO birthdate(person_name, birthdate) VALUES (?,?)";
    $STH = $this->DBH->prepare($sql);

    $result = $STH->execute($arrData);

     if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
     else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');


    }// end of store method



    public function index($fetchMode='ASSOC'){
        $sql = "SELECT * from birthdate where is_deleted = '0' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();






    public function view($fetchMode='ASSOC'){

        $sql = 'SELECT * from birthdate where id='.$this->id;

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();


    public function update(){

        $arrData = array ($this->person_name, $this->birthdate);
        $sql = "UPDATE birthdate SET person_name = ?, birthdate = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('index.php');

    }// end of update()


    public function delete(){

        $sql = "Delete from birthdate where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $result=$STH->execute();
        if($result)
            Message::message("Success! Data Has Been deleted ");
        else
            Message::message("Failed! Data Has Not Been deleted ");


        Utility::redirect('index.php');

    }// end of delete()


    public function trash(){

        $sql = "Update birthdate SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $result=$STH->execute();
        if($result)
            Message::message("Success! Data Has Been trashed");
        else
            Message::message("Failed! Data Has Not Been trashed");


        Utility::redirect('index.php');


    }// end of trash()

    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from birthdate where is_deleted <>'0' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();

    public function recover(){
        $sql='UPDATE birthdate  SET is_deleted  = "0" where id ='.$this->id;

        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute();
        if($result)
            Message::message(" Data Has Been recovered ");


        else
            Message::message("Data Hasn't Been recovered ");
        Utility::redirect('index.php');



    }









}// end of BookTitle Class

?>

