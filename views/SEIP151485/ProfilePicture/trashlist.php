<html>
<head>
<link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
    </head>
<body>
<h1> Trashed List</h1>

<?php
require_once("../../../vendor/autoload.php");



use App\ProfilePicture\ProfilePicture;
use App\Message\Message;
$objEmail = new ProfilePicture();
$someData=$objEmail->trashed("obj");
$serial=1;


echo "<table border='5px' >";

echo "<th> Serial </th>";
echo "<th> ID </th>";
echo "<th> Person name </th>";
echo "<th> profile picture </th>";
echo "<th> Action </th>";


foreach($someData as $oneData){      ########### Traversing $someData is Required for pagination  #############
    echo "<tr style='height: 40px'>";
    echo "<td>".$serial."</td>";

    echo "<td>".$oneData->id."</td>";
    echo "<td>".$oneData->person_name."</td>";
    echo "<td>".$oneData->profile_picture."</td>";


    echo "<td>";

    echo "<a href='recover.php?id=$oneData->id'><button class='btn btn-success'>Recover</button></a> ";

    echo "<a href='delete.php?id=$oneData->id'><button class='btn btn-danger'>Delete</button></a> ";


    echo "</td>";

    echo "</tr>";

    $serial++;
}

echo "</table>";


?>
</body>
</html>
