<html>
<head>
<link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
    </head>
<?php
require_once("../../../vendor/autoload.php");
use App\ProfilePicture\ProfilePicture;
use App\Message\Message;


$profilePicture=new ProfilePicture();
$dataSet=$profilePicture->index("obj");
$serial=1;
?>
<div class="container-fluid">
    <div class="table-responsive">
        <table border="2px" class="table table-bordered">
            <th>serial</th>
            <th>ID</th>
            <th>Person Name</th>
            <th>Profile Picture</th>
            <th>Action</th>
            <?php foreach ($dataSet as $oneData){?>
                <tr>
                    <td><?php echo $serial++; ?></td>
                    <td><?php echo $oneData->id; ?></td>
                    <td><?php echo $oneData->person_name; ?></td>
                    <td><?php echo $oneData->profile_picture; ?>
                        <br><img src="img/<?php echo $oneData->profile_picture; ?>" alt="Smiley face" height="40" width="40">

                    </td>


                    <td>
                        <a href='view.php?id=<?php echo $oneData->id ?>'><button class='btn btn-info'>View</button></a>
                        <a href='edit.php?id=<?php echo  $oneData->id ?>'><button class='btn btn-primary'>Edit</button></a>
                        <a href='trash.php?id=<?php echo  $oneData->id ?>'><button class='btn btn-warning'>trash</button></a>
                        <a href='delete.php?id=<?php echo  $oneData->id ?>'><button class='btn btn-danger'>Delete</button></a>
                    </td>
                </tr>
            <?php } ?>

        </table>
    </div>
</div>
</html>
