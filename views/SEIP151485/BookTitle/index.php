<html>
<head>
<link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
    </head>
<?php
require_once("../../../vendor/autoload.php");
use App\BookTitle\BookTitle;
use App\Message\Message;


$book=new BookTitle();
$dataSet=$book->index("obj");
$serial=1;
echo "<table border='1px' align='center'>";
echo "<th>serial</th><th>id</th><th>Book Title</th><th>Author Name</th><th>Action</th>";

foreach($dataSet as $oneData) {
    echo "<tr>";
    echo "<td>$serial</td>";
    echo "<td>$oneData->id</td>";
    echo "<td>$oneData->book_title</td>";
    echo "<td>$oneData->author_name</td>";

    $serial++;
    echo "
    <td>
    <a href='view.php?id=$oneData->id'><button class='btn-success'>view</button></a>
    <a href='edit.php?id=$oneData->id'><button class='btn-primary'>edit</button></a>
     <a href='trash.php?id=$oneData->id'><button class='btn-primary'>trash</button></a>
     <a href='delete.php?id=$oneData->id'><button class='btn-danger'>delete</button></a>
    </td>";
    echo "</tr>";

}
?>
</html>
